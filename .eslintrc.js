module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  //extends: "eslint:recommended",
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  rules: {
    nested_loop: "error",
    canvas: "error",
    if_return: "error",
    cache_variable: "error",
    html_image_resize: "error",
  },
  plugins: ["@html-eslint","css"],
  overrides: [
    {
      files: ["*.html", "*.css"],
      parser: "@html-eslint/parser",
      extends: ["plugin:css/recommended"],
    },
  ],
};
