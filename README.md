Create package.json and init the project with npm:

    npm init

Installing eslint globally in the root of the project:

    npm install -g eslint
Create eslint config files:

    eslint --init

Run eslint rules
    eslint . --rulesdir rules

Installing stylint
    https://stylelint.io/user-guide/get-started/
    
Installing postcss-html:
    npm install postcss-html --save-dev

Executing stylint
    npx stylelint "**/*[.css, .html]
 

Creating a custom rule
    https://www.codementor.io/@rudolfolah/stylelint-rules-how-to-write-your-own-rules-hhmwikafq



Autres sites utiles:
    https://astexplorer.net/
    
CSSAst Structure
    https://postcss.org/api/#declaration
